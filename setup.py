from setuptools import setup, find_packages


setup(
    name='peek',
    version='0.1',
    packages=find_packages(),
    author='Zoltan Szalai',
    author_email='flack@wsbricks.com',
    description='',
    url='https://bitbucket.org/flack/peek',
)
