Usage
=====

>>> from peek import get
>>> get("www.glackaudio.com")
{'content_type': 'text/html; charset=utf-8',
 'description': u'A Radio Bro 91.4 m\xe1jusban felforgatja a budapesti \xe9tert. Egy h\xf3napon kereszt\xfcl minden h\xe9tk\xf6znap a legjobb csapatok DJ-i zen\xe9lnek 21 \xf3r\xe1t\xf3l \xe9jf\xe9lig.',
 'hostname': 'glackaudio.com',
 'normalized_url': 'http://glackaudio.com/news/',
 'thumbnails': ['http://glackaudio.com/media/pic/glack_audio_logo.jpg',
  'http://glackaudio.com/media/pic/player_icon.jpg',
  'http://www.9stuff0.com/sites/default/files/rerror_380.jpg?1300796045'],
 'title': u'Glack Audio',
 'url': 'www.glackaudio.com'}


>>> from peek import Peek
>>> url = "http://www.youtube.com/watch?v=ZYIVlyEEk9E"
>>> p = Peek(url)
>>> p.get_data()
{'description': u'',
 'embed_src': 'https://www.youtube.com/v/ZYIVlyEEk9E?version=3&f=videos&app=youtube_gdata&autoplay=1&fs=1',
 'hostname': 'www.youtube.com',
 'normalized_url': 'http://www.youtube.com/watch?v=ZYIVlyEEk9E',
 'thumbnails': ['http://i.ytimg.com/vi/ZYIVlyEEk9E/0.jpg',
  'http://i.ytimg.com/vi/ZYIVlyEEk9E/1.jpg',
  'http://i.ytimg.com/vi/ZYIVlyEEk9E/2.jpg',
  'http://i.ytimg.com/vi/ZYIVlyEEk9E/3.jpg'],
 'title': u'BETA (UK) @ Hallasd a hangod Part III (Kecskem\xe9t, M\u0171kertv\xe1rosi Free Line BMX-p\xe1lya) 2010.03.26.',
 'url': 'http://www.youtube.com/watch?v=ZYIVlyEEk9E'}

>>> p.get_facebook_data()
{'caption': 'www.youtube.com',
 'description': u'',
 'href': 'http://www.youtube.com/watch?v=ZYIVlyEEk9E',
 'media': [{'alt': u'BETA (UK) @ Hallasd a hangod Part III (Kecskem\xe9t, M\u0171kertv\xe1rosi Free Line BMX-p\xe1lya) 2010.03.26.',
   'expanded_height': '300',
   'expanded_width': '398',
   'href': 'http://www.youtube.com/watch?v=ZYIVlyEEk9E',
   'imgsrc': 'http://i.ytimg.com/vi/ZYIVlyEEk9E/0.jpg',
   'src': 'http://i.ytimg.com/vi/ZYIVlyEEk9E/0.jpg',
   'swfsrc': 'https://www.youtube.com/v/ZYIVlyEEk9E?version=3&f=videos&app=youtube_gdata&autoplay=1&fs=1',
   'type': 'flash'}],
 'name': u'BETA (UK) @ Hallasd a hangod Part III (Kecskem\xe9t, M\u0171kertv\xe1rosi Free Line BMX-p\xe1lya) 2010.03.26.'}
