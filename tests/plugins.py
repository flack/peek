from unittest import TestCase


class PluginTestMixin(object):
    maxDiff = None
    plugin_class = None

    def test_plugin(self):
        for data in self.test_data:
            plugin = self.plugin_class(data['url'])
            response_data = plugin.check()
            for key, value in data['response'].items():
                self.assertEqual(response_data[key], value)
