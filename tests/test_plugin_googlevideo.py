from unittest import TestCase

from peek.plugins.googlevideo import GoogleVideoPlugin

from .plugins import PluginTestMixin


class Test(PluginTestMixin, TestCase):
    plugin_class = GoogleVideoPlugin
    test_data = (
        {
            'url': 'http://video.google.com/videoplay?docid=7065205277695921912',
            'response': {
                'title': 'Zeitgeist: Addendum',
                'description': 'Zeitgeist: Addendum, by Peter Joseph\n2008',
                'embed_src': 'http://video.google.com/googleplayer.swf?docid=7065205277695921912&hl=en&fs=true',
                'hostname': 'video.google.com',
                },
        },
        {
            'url': 'http://video.google.com/videoplay?docid=7866929448192753501',
            'response': {
                'title': 'Loose Change 2nd Edition Recut',
                'description': 'Dylan Avery, Korey Rowe, and Jason Bermas bring you the most powerful 9/11 Documentary yet. Updated!!!!',
                'embed_src': 'http://video.google.com/googleplayer.swf?docid=7866929448192753501&hl=en&fs=true',
                'hostname': 'video.google.com',
            },
        },
    )
