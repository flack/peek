from unittest import TestCase

from peek.plugins.metacafe import MetacafePlugin

from .plugins import PluginTestMixin


class Test(PluginTestMixin, TestCase):
    plugin_class = MetacafePlugin
    test_data = (
        {
            'url': 'http://www.metacafe.com/watch/7836036/dvdiva_top_10_holiday_movies/',
            'response': {
                'description': 'Brigitte gives you a run down of the best holiday movies of all time.',
                'embed_src': 'http://www.metacafe.com/fplayer/7836036/dvdiva_top_10_holiday_movies.swf',
                'thumbnails': ['http://s1.mcstatic.com/thumb/7836036/21186097/4/catalog_item5/0/1/dvdiva_top_10_holiday_movies.jpg?v=3'],
                'title': 'DVDiva Top 10 Holiday Movies',
                'embed_media_id': '7836036',
                'hostname': 'www.metacafe.com',
            }
        },
    )
