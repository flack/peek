from unittest import TestCase

from peek.plugins.breakcom import BreakPlugin

from .plugins import PluginTestMixin


class Test(PluginTestMixin, TestCase):
    plugin_class = BreakPlugin
    test_data = (
        {
            'url': 'http://www.break.com/index/this-kid-makes-banjo-playing-cool-2298328',
            'response': {
                'description': 'Now this is the type of banjo playing I can get behind, unlike the "Deliverance" kind.',
                'embed_src': 'http://embed.break.com/2298328',
                'thumbnails': ['http://media1.break.com/dnet/media/2012/2/8/368cf481-3365-412c-862f-bb1f69760325.jpg'],
                'title': 'This Kid Makes Banjo Playing Cool',
                'embed_media_id': '2298328',
            }
        },
    )
