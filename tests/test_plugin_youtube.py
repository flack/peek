from unittest import TestCase

from peek.plugins.youtube import YoutubePlugin

from .plugins import PluginTestMixin


class Test(PluginTestMixin, TestCase):
    plugin_class = YoutubePlugin
    test_data = (
        {
            'url': 'http://www.youtube.com/watch?v=qQXAHLLe35o&feature=g-like',
            'response': {
                'title': 'Electrotek - Superfly (DJ Zouk Club Edit) - FULL',
                'description': '1996 \n\nDistinctive Records 12"',
                'thumbnails': [
                    'http://i.ytimg.com/vi/qQXAHLLe35o/0.jpg',
                    'http://i.ytimg.com/vi/qQXAHLLe35o/1.jpg',
                    'http://i.ytimg.com/vi/qQXAHLLe35o/2.jpg',
                    'http://i.ytimg.com/vi/qQXAHLLe35o/3.jpg',
                    ],
                'embed_src': 'https://www.youtube.com/v/qQXAHLLe35o?version=3&f=videos&app=youtube_gdata&autoplay=1&fs=1',
                'embed_media_id': 'qQXAHLLe35o',
                'hostname': 'www.youtube.com',
                },
        },
    )
