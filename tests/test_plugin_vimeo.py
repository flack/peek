from unittest import TestCase

from peek.plugins.vimeo import VimeoPlugin

from .plugins import PluginTestMixin


class Test(PluginTestMixin, TestCase):
    plugin_class = VimeoPlugin
    test_data = (
        {
            'url': 'http://vimeo.com/7199178',
            'response': {
                'description': 'Tim Brown (1st AveMachine) & Christopher Hewitt (Knucklehead) team up for the the new release from Hecq Vs Exillion. Titled "Spheres Of Fury"<br />\n<br />\n<br />\nhttp://christopherhewitt.com/<br />\nhttp://www.tabrown.co.uk/',
                'embed_src': 'http://vimeo.com/moogaloop.swf?clip_id=7199178',
                'thumbnails': ['http://b.vimeocdn.com/ts/302/054/30205463_200.jpg'],
                'title': 'Hecq Vs Exillion - Spheres Of Fury',
                'embed_media_id': '7199178',
                'hostname': 'vimeo.com',
            }
        },
    )
