import sys
import re
import socket
import traceback
import httplib
import urlparse
import urllib2
import logging

import anyjson

from lxml import html
from lxml import etree

from django.utils.html import strip_tags
from django.utils.text import truncate_html_words
from django.utils.encoding import smart_unicode

from .utils import *
from .exceptions import GetRedirectError, GetUrlError, PluginError
from .conf import *
from .constants import *


# default socket timeout 10 seconds
socket.setdefaulttimeout(10)


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(levelname)5s - %(processName)15s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def try_plugins(url):
    hostname = urlparse.urlsplit(url).netloc

    for plugin_class_string in PLUGINS:
        try:
            plugin_class = import_plugin(plugin_class_string)
        except PluginImportError, e:
            logger.debug("Plugin import error: {0} | {1}".format(
                                                plugin_class_string, str(e))
                                                )
            continue

        plugin = plugin_class(url)
        try:
            media_data = plugin.check(hostname)
            if media_data is not None:
                logger.debug("plugin match: {0} | {1}".format(
                                                plugin_class.__name__, url)
                                                )
                return media_data
        except Exception, e:
            logger.debug("PluginError | url: {0} | {1} | {2}".format(
                                        url, str(e), traceback.format_exc()
                                        ))
            raise PluginError(e, url)

    logger.debug("No plugin match | {}".format(url))


def get_url_head_response(url):
    u = urlparse.urlsplit(url)
    conn = httplib.HTTPConnection(u.netloc)
    conn.request("HEAD", u.path)
    res = conn.getresponse()
    return res


def get_url_content_length(head_response):
    content_length = head_response.getheader('content-length')
    try:
        content_length = int(content_length)
    except (ValueError, TypeError):
        content_length = 0

    return content_length


def find_oembed(tree):
    oembed_url = title = embed_html = thumbnail = None

    try:
        oembed_url = tree.xpath('//link[@rel="alternate"][@type="application/json+oembed"]')[0].get('href')
        oembed_format = "json"
    except:
        oembed_url = None

    if not oembed_url:
        try:
            oembed_url = tree.xpath('//link[@rel="alternate"][@type="text/xml+oembed"]')[0].get('href')
            oembed_format = "xml"
        except:
            oembed_url = None

    if not oembed_url:
        try:
            oembed_url = tree.xpath('//link[@rel="alternate"][@type="application/xml+oembed"]')[0].get('href')
            oembed_format = "xml"
        except:
            oembed_url = None

    if oembed_url:
        # set oembed url
        oembed_url = "%s&maxwidth=%s&maxheight=%s&format=%s" % (oembed_url, EMBED_MAX_WIDTH, EMBED_MAX_HEIGHT, oembed_format)

        if oembed_format == "json":
            json_response = anyjson.loads(get_url(oembed_url).read())
            oembed_type = json_response.get('type', None)

            title = json_response.get('title', None)

            if oembed_type == "photo":
                thumbnail = json_response.get('url', None)
            else:
                embed_html = json_response.get('html', None)
                thumbnail = json_response.get('thumbnail_url', None)

        elif oembed_format == "xml":
            xml_tree = etree.parse(oembed_url)
            oembed_type = getattr(xml_tree.find("type"), 'text', None)

            title = getattr(tree.find("title"), 'text', None)

            if oembed_type == "photo":
                thumbnail = getattr(xml_tree.find("url"), 'text', None)
            else:
                embed_html = getattr(xml_tree.find("html"), 'text', None)
                thumbnail = getattr(xml_tree.find("thumbnail_url"), 'text', None)

    data = {}
    if title:
        data.update({'title': title})
    if thumbnail:
        data.update({'thumbnails': [thumbnail]})
    if embed_html:
        data.update({'embed_html': embed_html})

    logger.debug("oembed data: {}".format(data))
    return data


def process_response(response, exclude_small_images=True):
    # some things we'll need
    url = response.url
    url_parts = urlparse.urlsplit(url)
    netloc = url_parts.netloc
    base_url = urlparse.urlunsplit((url_parts.scheme, netloc, '', '', ''))

    # the data we will return
    data = {}

    # checking the content type
    content_type = get_content_type(response)
    data['content_type'] = content_type

    thumbnails = []
    if 'image' in content_type:
        if content_type in ('image/jpeg', 'image/png', 'image/gif'):
            thumbnails = [url]
        data.update({
            'title': netloc,
            'description': '',
            'thumbnails': thumbnails
        })
        return data

    elif 'audio' in content_type:
        embed_src = ''
        if content_type in ('audio/mpeg',):
            embed_src = url
        try:
            audio_title = url_parts.path.split("/")[-1]
            audio_title = urllib2.unquote(audio_title)
            if "." in audio_title:
                audio_title = audio_title.rsplit(".", 1)[0]
            audio_title = re.sub(r"[_]+", " ", audio_title)
        except:
            audio_title = netloc
        data.update({
            'title': audio_title,
            'description': '',
            'thumbnails': [],
            'embed_src': embed_src,
        })
        return data

    elif 'text' not in content_type:
        data = {
            'title': netloc,
            'description': '',
            'thumbnails': [],
        }
        return data

    # parse html
    tree = html_parse_response(response).getroot()

    oembed_data = find_oembed(tree)

    if oembed_data.get('title', ''):
        title = oembed_data['title']
    else:
        # get page title
        title = tree.findtext("head/title")
        if not title:
            try:
                title = tree.xpath('//meta[@name="title"]')[0].get('content')
            except:
                title = netloc

    if oembed_data.get('description', ''):
        description = oembed_data['description']
    else:
        # get page description
        description = u''
        try:
            description = tree.xpath('//meta[@name="description"]')[0].get('content', '')
        except IndexError:
            try:
                description = tree.xpath('//meta[@name="Description"]')[0].get('content', '')
            except IndexError:
                # XXX better algorithm needed
                # if meta description is not definied we choose the first
                # (div or p) text node that
                # contains more characters than MIN_TEXT_NODE_LENGTH
                text_list = tree.xpath('//div/text()') + tree.xpath('//p/text()')
                for t in text_list:
                    nt = t.strip()
                    if len(nt) > MIN_TEXT_NODE_LENGTH:
                        description = nt
                        break

    if 'thumbnails' in oembed_data:
        thumbnails = oembed_data['thumbnails']
    else:
        # get page images
        # first try image_src
        try:
            image = tree.xpath('//link[@rel="image_src"]')[0].get('href')
            thumbnails = [image]
        except IndexError:
            # get page images
            skipped_images = []
            # image counter
            i = 0
            for img in tree.getiterator('img'):
                if i >= MAX_IMAGE_NUM:
                    break
                img_url = img.get('src')
                if not img_url:
                    continue
                if urlparse.urlsplit(img_url).scheme == '':
                    img_url = urlparse.urljoin(base_url, img_url)

                if exclude_small_images:
                    # filter out too small images
                    head_response = get_url_head_response(img_url)
                    cl = get_url_content_length(head_response)
                    if cl < MIN_IMAGE_SIZE:
                        continue

                # filter out images appear more than once
                if img_url not in skipped_images:
                    try:
                        thumbnails.remove(img_url)
                        skipped_images.append(img_url)
                        i -= 1
                    except ValueError:
                        thumbnails.append(img_url)
                        i += 1

    # what about domain brokers?
    data.update({
        'title': title,
        'description': description,
        'thumbnails': thumbnails
    })

    if 'embed_html' in oembed_data:
        data['embed_html'] = oembed_data['embed_html']

    return data


class Peek(object):

    def __init__(self, url):
        self.url = url

    def _get_from_normalized_url(self):
        normalized_url = self.normalized_url

        redirect_url = normalized_url
        data = None
        while True:
            try:
                data = try_plugins(normalized_url)

            except PluginError, e:
                data = None

            if data is not None:
                break
            else:
                # try to follow redirects
                try:
                    redirect_url = get_redirect(normalized_url)
                    if redirect_url is not None:
                        normalized_url = redirect_url
                    else:
                        break
                except GetRedirectError, e:
                    logger.debug("GetRedirectError | url: {0} | {1}".format(
                                                    normalized_url, str(e)
                                                    ))
                    break

        if data is None:
            # we try the normal process if still no data
            logger.debug("normal get url process")
            response = get_url(normalized_url)
            # we had to call get_url so we can use response.url for
            # normalized url
            normalized_url = response.url
            data = process_response(response)
            logger.debug("process response data: {}".format(data))

        # make sure no html tags in the title and description
        if 'title' not in data or data['title'] is None:
            title = normalized_url
        else:
            title = strip_tags(data['title']).strip()

        if 'description' not in data or data['description'] is None:
            description = ''
        else:
            description = strip_tags(
                            truncate_html_words(data['description'].strip(),
                                                MAX_DESCRIPTION_WORDS)
                          )

        if 'hostname' in data:
            hostname = data['hostname']
        else:
            hostname = get_hostname(normalized_url)

        ret_data = {
            'url': self.url,
            'normalized_url': normalized_url,
            'hostname': hostname,
            'title': title,
            'description': description,
            'thumbnails': data.get('thumbnails', []),
        }

        if 'content_type' in data:
            ret_data['content_type'] = data['content_type']
        if 'embed_src' in data:
            ret_data['embed_src'] = data['embed_src']
        if 'embed_html' in data:
            # sanitize it
            ret_data['embed_html'] = sanitize_embed_html(data['embed_html'])

        return ret_data

    def normalize_url(self):
        normalized_url = normalize_url(self.url)
        self.normalized_url = normalized_url
        return normalized_url

    def get_data(self):
        normalized_url = self.normalize_url()

        try:
            return self._get_from_normalized_url()
        except Exception, e:
            logger.debug(traceback.format_exc())

            return {
                'url': self.url,
                'normalized_url': normalized_url,
                'title': normalized_url,
                'description': '',
                'thumbnails': []
            }

    def get_hostname(self, url):
        return urlparse.urlsplit(self.normalized_url).netloc

    def transform_to_facebook(self, data, thumbnail=None):
        title = data['title']
        description = data['description']
        normalized_url = data['normalized_url']
        hostname = data['hostname']
        thumbnails = data['thumbnails']
        if thumbnail in thumbnails:
            thumbnail = thumbnail
        else:
            try:
                thumbnail = thumbnails[0]
            except:
                thumbnail = DEFAULT_MEDIA_IMAGE_URL

        attachment = {
            'name': title,
            'description': description,
            'caption': hostname,
            'href': normalized_url,
            'media': [{}]
        }

        if 'content_type' in data and data['content_type'] == 'audio/mpeg':
            attachment['media'] = [{
                "type": "mp3",
                "src": normalized_url,
                "title": title,
                "artist": "",
                "album": "",
            }]
            return attachment

        if data['thumbnails']:
            attachment['media'] = [{
                'type': 'image',
                'src': thumbnail,
                'alt': title,
                'href': normalized_url,
            }]

        embed_src = data.get('embed_src', None)
        if embed_src:
            attachment['media'][0]['type'] = 'flash'
            attachment['media'][0]['swfsrc'] = embed_src
            attachment['media'][0]['imgsrc'] = thumbnail
            #attachment['media'][0]['height'] = '100'
            #attachment['media'][0]['width'] = '100'
            attachment['media'][0]['expanded_height'] = \
                            str(EMBED_MAX_HEIGHT)
            attachment['media'][0]['expanded_width'] = \
                            str(EMBED_MAX_WIDTH)

        return attachment

    def get_facebook_data(self, data=None, thumbnail=None):
        if data is None:
            data = self.get_data()
        return self.transform_to_facebook(data, thumbnail)


def get(url):
    p = Peek(url)
    return p.get_data()
