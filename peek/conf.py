#URL_VALIDATOR_USER_AGENT = "peek_urlbot/1.0 (+http://peek.com/urlbot/)"
URL_VALIDATOR_USER_AGENT = "peek_urlbot/1.0"
MAX_IMAGE_NUM = 10
MIN_TEXT_NODE_LENGTH = 100
# 2kB
MIN_IMAGE_SIZE = 512
MAX_DESCRIPTION_WORDS = 120
DEFAULT_MEDIA_IMAGE_URL = 'default_media.png'


BUILTIN_PLUGINS = (
    'peek.plugins.youtube.YoutubePlugin',
    'peek.plugins.vimeo.VimeoPlugin',
    'peek.plugins.metacafe.MetacafePlugin',
    'peek.plugins.breakcom.BreakPlugin',
    'peek.plugins.myvideo.MyvideoPlugin',
    'peek.plugins.googlevideo.GoogleVideoPlugin',
    'peek.plugins.guba.GubaPlugin',
    'peek.plugins.liveleak.LiveleakPlugin',
    'peek.plugins.livevideo.LiveVideoPlugin',
    'peek.plugins.sapopt.SapoptPlugin',
    'peek.plugins.indavideo.IndavideoPlugin',
    'peek.plugins.pixterhu.PixterhuPlugin',
)

EXTRA_PLUGINS = ()

PLUGINS = BUILTIN_PLUGINS + EXTRA_PLUGINS
