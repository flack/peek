from .base import PluginBase


class LiveleakPlugin(PluginBase):
    domains = ('liveleak.com',)
    regex = r"^(http://)?(www\.)?liveleak\.com/view\?i=(?P<mid>[A-Za-z0-9_]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        thumbnail = tree.xpath('//link[@rel="videothumbnail"]')[0].get('href')
        embed_src = "http://www.liveleak.com/e/%s" % video_id

        data = {
            'thumbnails': [thumbnail],
            'embed_src': embed_src,
        }
        return data
