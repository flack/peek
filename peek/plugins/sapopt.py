from .base import PluginBase


class SapoptPlugin(PluginBase):
    domains = (
        'videos.sapo.pt',
        #'sapopt.com',
    )
    regex = r"^(http://)?videos.sapo\.pt/(?P<mid>[A-Za-z0-9]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        try:
            title = tree.xpath('//div[@id="topplayer"]/div[@class="tit"]')[0].text
        except Exception, e:
            title = None

        try:
            description = tree.xpath('//div[@id="lksvideo"]/div[@class="bk"]/div[@class="desc"]')[0].text
        except Exception, e:
            description = None

        embed_src = "http://rd3.videos.sapo.pt/play?file=http://rd3.videos.sapo.pt/%s/mov/1" % video_id

        data = {
            'embed_src': embed_src,
        }
        return data
