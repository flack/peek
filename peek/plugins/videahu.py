from lxml import etree

from .base import PluginBase
from ..utils import strip_tags


class VideahuPlugin(PluginBase):
    domains = ('videa.hu',)
    regex = r"^(http://)?(www\.)?videa\.hu/videok/[A-Za-z0-9_\-/]*[A-Za-z0-9\-\.]*-(?P<mid>[A-Za-z0-9]+)"

    def get_media_data(self, video_id):
        embed_src = "http://videa.hu/flvplayer.swf?v=%s" % video_id

        data = {
            'embed_src': embed_src,
        }
        return data
