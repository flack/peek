from .base import PluginBase


class MyvideoPlugin(PluginBase):
    domains = ('myvideo.',)  # for all myvideo sites (.de, .hu)
    regex = r"^(http://)?(www\.)?myvideo\.(de|hu)/watch/(?P<mid>[0-9]+)"

    def get_media_data(self, video_id):
        hostname = self.get_hostname()
        embed_src = "http://%s/movie/%s" % (hostname, video_id)

        data = {
            'embed_src': embed_src,
        }
        return data
