from .base import PluginBase


class YfrogPlugin(PluginBase):
    domains = ('yfrog.com',)
    regex = r"^(http://)?(www\.)?yfrog\.com/(?P<mid>[a-z0-9]+)"

    def get_media_data(self, media_id):
        thumbnail = 'http://yfrog.com/%s.th.jpg' % media_id

        data = {
            'thumbnails': [thumbnail],
        }
        return data
