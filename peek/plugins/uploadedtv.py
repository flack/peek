from .base import PluginBase


class UploadedtvPlugin(PluginBase):
    domains = ('uploaded.tv',)
    regex = r"^(http://)?(www\.)?uploaded\.tv/video/(?P<mid>[0-9]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        try:
            title = tree.xpath('//h2[@class="media-name"]')[0].text
        except Exception, e:
            title = None
        try:
            description = tree.xpath('//div[@class="desp restrictLine"]')[1].text_content()
        except Exception, e:
            description = None

        embed_src = "http://uploaded.tv/embed/embed.swf?id=%s&displaymode=V" % video_id

        data = {
            'title': title,
            'description': description,
            'embed_src': embed_src,
        }
        return data
