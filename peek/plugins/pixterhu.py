from .base import PluginBase


class PixterhuPlugin(PluginBase):
    domains = ('pixter.hu',)
    regex = r"^(http://)?(www\.)?pixter\.hu/video\?id=(?P<mid>[0-9]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        try:
            title = tree.xpath('//h1[@id="edit_title"]')[0].text
        except Exception, e:
            title = None

        try:
            description = tree.xpath('//div[@class="left_box"]')[0].text_content()
        except Exception, e:
            description = None

        thumbnails = []
        for i in range(1, 5):
            thumbnails.append('http://static.pixter.hu/thumbnail/%s_%d_140x104.jpg' % (video_id, i))

        embed_src = "http://static.pixter.hu/video_player/pixter_player_embedfs.swf?file=http://static.pixter.hu/playlist2_%s.xml" % video_id

        data = {
            'title': title,
            'description': description,
            'thumbnails': thumbnails,
            'embed_src': embed_src,
        }
        return data
