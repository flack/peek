from .base import PluginBase


class GubaPlugin(PluginBase):
    domains = ('guba.com',)
    regex = r"^(http://)?(www\.)?guba\.com/watch/(?P<mid>[0-9]+)"

    def get_media_data(self, video_id):
        tree = html_parse_url(url).getroot()

        try:
            description = tree.xpath('//div[@id="guts2"]/p[@class="caption"]')[0].text
        except Exception, e:
            description = None

        embed_src = "http://www.guba.com/f/root.swf?video_url=http://free.guba.com/uploaditem/%s/flash.flv&isEmbeddedPlayer=true" % video_id

        data = {
            'description': description,
            'embed_src': embed_src,
        }
        return data
