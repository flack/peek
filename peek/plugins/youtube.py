import gdata.youtube.service

from .base import PluginBase


class YoutubePlugin(PluginBase):
    domains = ('youtube.com', 'youtu.be')
    regex = r"^(http://)?(www\.)?youtube\.com/(watch\?v=|v/)(?P<mid>[A-Za-z0-9\-=_]{11})"

    def get_media_data(self, video_id):
        yt_service = gdata.youtube.service.YouTubeService()
        entry = yt_service.GetYouTubeVideoEntry(video_id=video_id)

        thumbnails = [t.url for t in entry.media.thumbnail]
        embed_src = entry.GetSwfUrl()
        if embed_src:
            #embed_src = "http://www.youtube.com/v/%s&autoplay=1&fs=1" % video_id
            embed_src = embed_src + '&autoplay=1&fs=1'

        data = {
            'title': entry.media.title.text,
            'description': entry.media.description.text,
            'thumbnails': thumbnails,
            'embed_src': embed_src,
        }
        return data
