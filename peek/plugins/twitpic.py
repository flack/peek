from .base import PluginBase


class TwitpicPlugin(PluginBase):
    domains = ('twitpic.com',)
    regex = r"^(http://)?(www\.)?twitpic\.com/(?P<mid>[A-Za-z0-9]+)"

    def get_media_data(self, media_id):
        thumbnail = 'http://twitpic.com/show/thumb/%s' % media_id

        data = {
            'thumbnails': [thumbnail],
        }
        return data
