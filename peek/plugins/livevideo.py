from .base import PluginBase


class LiveVideoPlugin(PluginBase):
    domains = ('livevideo.com',)
    regex = r"^(http://)?(www\.)?livevideo\.com/video/[A-Za-z0-9_\-/]*(?P<mid>[A-Za-z0-9]{32})/[A-Za-z0-9\-\_]+\.aspx"

    def get_media_data(self, video_id):
        # embed_src = tree.xpath('//link[@rel="video_src"]')[0].get('href')
        embed_src = "http://www.livevideo.com/flvplayer/embed/%s" % video_id

        data = {
            'embed_src': embed_src,
        }
        return data
