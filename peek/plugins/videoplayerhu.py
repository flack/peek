from lxml import etree

from .base import PluginBase
from ..utils import strip_tags


class VideoPlayerhuPlugin(PluginBase):
    domains = ('videoplayer.hu',)
    regex = r"^(http://)?(www\.)?videoplayer\.hu/videos/play/(?P<mid>[0-9]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        try:
            title = tree.xpath("//h4")[0].text
        except Exception, e:
            title = None

        try:
            description = strip_tags(tree.xpath('//div[@id="column_right"]/div[@class="box_border"]/p')[1].text_content().strip())
        except Exception, e:
            description = None

        embed_src = "http://www.videoplayer.hu/videos/embed/%s" % video_id

        data = {
            'title': title,
            'description': description,
            'embed_src': embed_src,
        }
        return data
