from .base import PluginBase


class BreakPlugin(PluginBase):
    domains = ('break.com',)
    regex = r"^(http://)?(www\.)?break\.com/[A-Za-z0-9\-_/]+\-(?P<mid>[A-Za-z0-9_\-]+)(\.html)?"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        title = tree.xpath('//meta[@name="embed_video_title"]')[0].get('content')
        description = tree.xpath('//meta[@name="embed_video_description"]')[0].get('content')
        thumbnail = tree.xpath('//meta[@name="embed_video_thumb_url"]')[0].get('content')
        embed_src = tree.xpath('//meta[@name="embed_video_url"]')[0].get('content')

        data = {
            'title': title,
            'description': description,
            'thumbnails': [thumbnail],
            'embed_src': embed_src,
        }
        return data
