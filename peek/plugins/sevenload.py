from .base import PluginBase


class SevenloadPlugin(PluginBase):
    domains = ('sevenload.com',)
    regex = r"^(http://)?(www\.)?\w{2}.sevenload\.com/videos/(?P<mid>[A-Za-z0-9]+)"

    def get_media_data(self, video_id):
        #embed_src = tree.xpath('//link[@rel="video_src"]')[0].get('href')
        embed_src = "http://en.sevenload.com/pl/%s/425x350/swf" % video_id

        data = {
            'embed_src': embed_src,
        }
        return data
