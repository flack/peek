import urlparse

from lxml import etree

from ..utils import *
from ..conf import *


class PluginBase(object):
    domains = []
    regex = r""
    media_id_group_name = 'mid'

    def __init__(self, url):
        self._data_attrs = (
            'title', 'description', 'thumbnails', 'embed_src',
            )
        for attr in self._data_attrs:
            setattr(self, "_{}".format(attr), None)
        self._tree = None
        self._hostname = None
        self.url = url

    def _get_default_title(self):
        tree = self.get_tree()
        assert tree is not None
        if self._title is None:
            try:
                title = tree.xpath('//meta[@name="title"]')[0].get('content')
            except IndexError, e:
                try:
                    title = tree.xpath('//meta[@name="Title"]')[0].get('content')
                except IndexError, e:
                    title = tree.findtext("head/title")
            self._title = title.strip()
        return self._title

    def get_title(self):
        return self._get_default_title()

    def _get_default_description(self):
        tree = self.get_tree()
        assert tree is not None
        if self._description is None:
            try:
                description = tree.xpath('//meta[@name="description"]')[0].get('content')
            except IndexError, e:
                try:
                    description = tree.xpath('//meta[@name="Description"]')[0].get('content')
                except IndexError, e:
                    description = self.get_title()
            self._description = description.strip()
        return self._description

    def get_description(self):
        return self._get_default_description()

    def _get_default_thumbnails(self):
        tree = self.get_tree()
        assert tree is not None
        if self._thumbnails is None:
            try:
                thumbnails = [tree.xpath('//link[@rel="image_src"]')[0].get('href')]
            except Exception, e:
                thumbnails = []
            self._thumbnails = thumbnails
        return self._thumbnails

    def get_thumbnails(self):
        return self._get_default_thumbnails()

    def _get_default_embed_src(self):
        tree = self.get_tree()
        assert tree is not None
        if self._embed_src is not None:
            try:
                embed_src = tree.xpath('//link[@rel="video_src"]')[0].get('href')
            except Exception, e:
                embed_src = None
            self._embed_src = embed_src
        return self._embed_src

    def get_embed_src(self):
        return self._get_default_embed_src()

    def get_tree(self, force_encoding=None):
        if self._tree is None:
            self._tree = html_parse_url(self.url, force_encoding).getroot()
        return self._tree

    def get_media_data(self, media_id):
        data = {}
        for attr in self._data_attrs:
            method = getattr(self, 'get_{}'.format(attr))
            data[attr] = method()
        data['embed_media_id'] = media_id
        return data

    def match(self):
        match = re.compile(self.regex).match(self.url)
        if match:
            media_id = match.group(self.media_id_group_name)
            media_data = self.get_media_data(media_id)
            media_data['embed_media_id'] = media_id
            for attr in self._data_attrs:
                if attr not in media_data or media_data[attr] is None:
                    method = getattr(self, '_get_default_{}'.format(attr))
                    media_data[attr] = method()
            if ('thumbnails' not in media_data or
                not media_data['thumbnails']):
                # if the provider specific function couldn't get
                # any thumbnails for the url we use a default one
                media_data['thumbnails'] = [DEFAULT_MEDIA_IMAGE_URL]
            media_data['hostname'] = self.get_hostname()
            return media_data

    def get_hostname(self):
        if self._hostname is None:
            self._hostname = get_hostname(self.url)
        return self._hostname

    def check(self, hostname=None):
        if hostname is None:
            hostname = self.get_hostname()

        if self.domains:
            for domain in self.domains:
                if domain in hostname:
                    return self.match()

        return self.match()
