from .base import PluginBase


class GoogleVideoPlugin(PluginBase):
    domains = ('video.google',)
    regex = r"^(http://)?video\.google\.(?:[A-Za-z\.]{2,5})/videoplay\?docid=(?P<mid>[0-9\-]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        try:
            title = tree.xpath('//div[@id="video-title"]')[0].text
        except IndexError:
            try:
                title = tree.xpath('//span[@id="details-title"]')[0].text
            except IndexError, e:
                title = None

        try:
            description = tree.xpath('//span[@id="video-description"]')[0].text
        except:
            try:
                description = tree.xpath('//span[@id="long-desc"]')[0].text
            except:
                description = None

        embed_src = "http://video.google.com/googleplayer.swf?docid=%s&hl=en&fs=true" % video_id

        data = {
            'title': title,
            'description': description,
            'embed_src': embed_src,
        }
        return data
