from .base import PluginBase


class StickamPlugin(PluginBase):
    domains = ('stickam.com',)
    regex = r"^(http://)?(www\.)?stickam\.com/viewMedia.do\?mId=(?P<mid>[0-9]+)"

    def get_media_data(self, video_id):
        #embed_code = tree.xpath('//input[@id="%s"]' % video_id)[0].get('value')
        #embed_src = etree.HTML(embed_code).xpath('//embed')[0].get('src')
        #embed_src = "http://player.stickam.com/flashVarMediaPlayer/%s" % video_id
        embed_src = "http://player.stickam.com/stickamPlayer/mp/%s" % video_id

        data = {
            'embed_src': embed_src,
        }
        return data
