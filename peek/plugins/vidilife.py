from lxml import etree

from .base import PluginBase


class VidilifePlugin(PluginBase):
    domains = ('vidilife.com',)
    regex = r"^(http://)?(www\.)?vidilife\.com/video_play_(?P<mid>[0-9]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        embed_code = tree.xpath('//input[@name="url1"]')[0].get('value')
        embed_src = etree.HTML(embed_code).xpath('//embed[@id="vidilife_movie"]')[0].get('src')

        data = {
            'embed_src': embed_src,
        }
        return data
