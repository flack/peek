from .base import PluginBase


class YvhuPlugin(PluginBase):
    domains = ('yv.hu',)
    regex = r"^(http://)?(www\.)?yv\.hu/(play_a_video\.html[A-Za-z0-9\?=&]+uploadedfileidhash|p)=(?P<mid>[A-Za-z0-9]+)"

    def get_media_data(self, video_id):
        tree = self.get_tree()

        try:
            title = tree.xpath('//div[@id="div_title"]')[0].text
        except:
            title = None

        try:
            description = tree.xpath('//div[@id="div_description"]')[0].text
        except:
            description = None

        thumbnail_url = 'http://img.yv.hu/thumbnail/%s/%s/%s/%s_small_16_9.jpg' % (video_id[0], video_id[1], video_id[2], video_id)
        thumbnails = [thumbnail_url]
        embed_src = 'http://www.yv.hu/v=%s' % video_id

        data = {
            'title': title,
            'description': description,
            'thumbnails': thumbnails,
            'embed_src': embed_src,
        }
        return data
