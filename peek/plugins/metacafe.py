from lxml import etree

from .base import PluginBase


class MetacafePlugin(PluginBase):
    domains = ('metacafe.com',)
    regex = r"^(http://)?(www\.)?metacafe\.com/watch/(?P<mid>([0-9]+|yt-[A-Za-z0-9\-=_]{11}))/"

    def get_media_data(self, video_id):
        api_url = "http://www.metacafe.com/api/item/%s" % video_id
        tree = etree.parse(api_url).getroot()
        media_ns = tree.nsmap.get("media")

        title = tree.find("channel/item/{%s}title" % media_ns).text
        description = tree.find("channel/item/{%s}description" % media_ns).text
        thumbnail = tree.find("channel/item/{%s}thumbnail" % media_ns).get("url")
        embed_src = tree.find("channel/item/{%s}content" % media_ns).get("url")

        data = {
            'title': title,
            'description': description,
            'thumbnails': [thumbnail],
            'embed_src': embed_src,
        }
        return data
