from lxml import etree

from .base import PluginBase


class VimeoPlugin(PluginBase):
    domains = ('vimeo.com',)
    regex = r"^(http(s)?://)?(www\.)?vimeo\.com/(?P<mid>[0-9]+)$"

    def get_media_data(self, video_id):
        api_url = "http://vimeo.com/api/v2/video/%s.xml" % video_id
        tree = etree.parse(api_url)

        title = tree.find("video/title").text
        description = tree.find("video/description").text
        thumbnail = tree.find("video/thumbnail_medium").text
        embed_src = "http://vimeo.com/moogaloop.swf?clip_id=%s" % video_id

        data = {
            'title': title,
            'description': description,
            'thumbnails': [thumbnail],
            'embed_src': embed_src,
        }
        return data
