from lxml import etree

from .base import PluginBase


class IndavideoPlugin(PluginBase):
    domains = ('indavideo.hu',)
    regex = r"^(http://)?[A-Za-z0-9-_\.]*indavideo\.hu/video/(?P<mid>[A-Za-z0-9_\-]+)"

    def get_media_data(self, video_id):
        # XXX indavideo does not send proper encoding, we force it to utf-8
        tree = self.get_tree(force_encoding='utf-8')

        desc = tree.xpath('//meta[@name="description"]')
        description = desc[0].get('content', '')
        try:
            description = description.split(" - ")[1]
        except IndexError:
            pass

        embed_code = tree.xpath('//input[@id="video-embed"]')[0].get('value')
        src = etree.HTML(embed_code).xpath('//embed')[0].get('flashvars')
        video_id = src.split('&')[0].split("=")[1]
        embed_src = "http://files.indavideo.hu/player/gup.swf?b=1009&vID=%s&autostart=1" % video_id

        data = {
            'description': description,
            'embed_src': embed_src,
        }
        return data
