import re
import urllib2
import urlparse

from lxml import html

from ..exceptions import GetRedirectError, GetUrlError
from ..conf import URL_VALIDATOR_USER_AGENT


def parse_qs(query):
    return dict((q.split("=") for q in query.split("&")))


def get_encoding(content_type):
    encoding = None
    match = re.compile(r'charset=(?P<enc>[a-zA-Z0-9-]+)').search(content_type)
    if match:
        encoding = match.group('enc')
    return encoding


def get_content_type(response):
    content_type = response.info()['content-type']
    return content_type


def get_redirect(url):
    # based on: http://github.com/joshthecoder/shorty-python/blob/master/shorty.py

    class StopRedirectHandler(urllib2.HTTPRedirectHandler):

        def http_error_301(self, req, fp, code, smg, headers):
            return None

        def http_error_302(self, req, fp, code, smg, headers):
            return None

    o = urllib2.build_opener(StopRedirectHandler())
    try:
        o.open(url)
    except urllib2.HTTPError, e:
        if e.code == 301 or e.code == 302:
            return e.headers['Location']
        else:
            raise GetRedirectError(e, url)
    except urllib2.URLError, e:
        raise GetRedirectError(e, url)
    return None


def normalize_url(url):
    # based on the clean method of Django's forms.URLField

    # If no URL scheme given, assume http://
    if url and '://' not in url:
        url = u'http://%s' % url
    # If no URL path given, assume /
    if url and not urlparse.urlsplit(url)[2]:
        url += '/'
    if url == u'':
        return None

    return url


def get_url(url):
    """
    based on the clean method of Django's forms.URLField
    """

    headers = {
        "Accept": "text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5",
        "Accept-Language": "en-us,en;q=0.5",
        "Accept-Charset": "utf-8,ISO-8859-1;q=0.7,*;q=0.7",
        "Connection": "close",
        "User-Agent": URL_VALIDATOR_USER_AGENT,
    }

    try:
        str(url)
    except UnicodeEncodeError:
        url = url.encode("utf-8")

    try:
        req = urllib2.Request(url, None, headers)
        response = urllib2.urlopen(req)
    except ValueError, e:
        #traceback.print_exc(file=sys.stderr)
        raise GetUrlError(e, url)
    except Exception, e: # urllib2.URLError, httplib.InvalidURL, etc.
        #traceback.print_exc(file=sys.stderr)
        raise GetUrlError(e, url)

    return response


def html_parse_response(response, force_encoding=None):
    if force_encoding is not None:
        encoding = force_encoding
    else:
        # we try to find out the encoding from the content type
        content_type = get_content_type(response)
        encoding = get_encoding(content_type)
    #if encoding is None:
    #    return html.parse(response)
    # if we don't get a proper response header for encoding we assume utf-8
    if not encoding:
        encoding = 'utf-8'
    try:
        parser = html.HTMLParser(encoding=encoding)
    except LookupError:  # unknown encoding
        return html.parse(response)
    return html.parse(response, parser=parser)


def html_parse_url(url, force_encoding=None):
    response = get_url(url)
    return html_parse_response(response, force_encoding=force_encoding)


def get_hostname(url):
    return urlparse.urlsplit(url).netloc
