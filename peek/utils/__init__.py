from importlib import import_module

from .sanitize import *
from .url import *
from ..exceptions import PluginImportError


def import_class(class_string):
    module, classname = class_string.rsplit(".", 1)
    m = import_module(module)
    return getattr(m, classname)


def import_plugin(plugin):
    try:
        return import_class(plugin)
    except Exception, e:
        raise PluginImportError(str(e))
