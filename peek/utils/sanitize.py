import re

from django.utils.html import strip_spaces_between_tags
from django.utils.html import strip_tags

from BeautifulSoup import BeautifulSoup, Comment


def sanitize_html(value, valid_tags=None, valid_attrs=None):
    """
    based on the code at: http://stackoverflow.com/questions/16861/sanitising-user-input-using-python
    """
    r = re.compile(r'[\s]*(&#x.{1,7})?'.join(list('javascript:')))
    if valid_tags is None:
        valid_tags = 'p i strong b u a h1 h2 h3 pre br img'.split()
    if valid_attrs is None:
        valid_attrs = 'href src'.split()

    soup = BeautifulSoup(value)
    for comment in soup.findAll(text=lambda text: isinstance(text, Comment)):
        comment.extract()
    for tag in soup.findAll(True):
        if tag.name not in valid_tags:
            tag.hidden = True
        tag.attrs = [(attr, r.sub('', val)) for attr, val in tag.attrs
                     if attr in valid_attrs]

    value = soup.renderContents().decode('utf8')
    value = strip_spaces_between_tags(value)
    return value


def sanitize_embed_html(value):
    embed_html = sanitize_html(value,
                    valid_tags = ['object', 'param', 'embed'],
                    valid_attrs = ['height', 'width', 'flashvars',
                        'quality', 'style', 'name', 'wmode', 'autoplay',
                        'value', 'allowscriptaccess', 'src', 'type',])

    # strip text between tags
    embed_html = re.sub(r'>[^<]*?<', '><', embed_html)
    # remove text from the front
    embed_html = re.sub(r'^[^<]*<', '<', embed_html)
    # remove text from the end
    embed_html = re.sub(r'>[^<]*$', '>', embed_html)
    return embed_html
