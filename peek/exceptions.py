class ReasonUrlError(Exception):

    def __init__(self, reason, url):
        self.reason = str(reason)
        self.url = url

    def __str__(self):
        return "%s: %s" % (self.url, repr(self.reason))


class GetRedirectError(ReasonUrlError):
    pass


class GetUrlError(ReasonUrlError):
    pass


class PluginError(ReasonUrlError):
    pass


class PluginImportError(Exception):
    pass

